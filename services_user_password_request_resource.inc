<?php

/**
 * Resource for password email.
 */
function services_user_password_request_resource($name) {
	// Adds backwards compatibility with regression fixed in #1083242
	$name = _services_arg_value($name, 'name');

	// Load the required includes for requesting user's password.
	module_load_include('inc', 'user', 'user.pages');

	// Request password.
	//$form_state['values'] = $name;
	$form_state['values']['name'] = $name;
	$form_state['values']['op'] = 'E-mail new password';

	// Execute the register form.
	drupal_form_submit('user_pass', $form_state);

	if ($errors = form_get_errors()) {
		return service_error(implode(" ", $errors), 406, array('form_errors' => $errors));
	}
	else {
		return TRUE;
	}
}
